## **camera_image_analysis**

Set of procedures to obtain spectra from images acquired on monochrome CCD/CMOS camera for spectroscopy.

**Table of Contents**

[TOC]

---

##### Requirements

+ `IgorPro 7 and higher`



##### Tested with

+ `IgorPro 7.0.8`


-----


### Available functions

#### analyse_image_1bin.ipf

```
generate2D_from_image_batch ( folder_name )

generate2D_from_image ( input_image, output_folder,  col_start, col_end, col_start2, col_end2 , [remove_first] )

get_spectra_layered (ccd_image , wave_prefix  , col_start, col_end, col_start2, col_end2)

get_spectra (ccd_image ,  col_start, col_end, col_start2, col_end2)

```
#### Analyse_image_data.ipf

```
RMN_Anlys_gen2D_images_1bin ( bin1_Start, bin1_End )

RMN_Anlys_process_image_1bm ( input_image, output_folder,  col_start, col_end, noteString1, appendString, [remove_first] )

RMN_Anlys_processImg_1bm (ccd_image ,  col_start, col_end, appendString, noteString )


```
